## @file main.py
#  Python source file to use blah blah blah blah
#
#  @package main
#  blah blah blah blah description

import utime
import pyb
from motordriver import *
from encoder import Encoder
from kpcontrol import KPControl

# Initializing motor pins
pin_en = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000)

## A motor driver object
motor1 = MotorDriver(pin_en, pin_IN1, pin_IN2, tim)

# Initializing encoder pins
pin_B6 = pyb.Pin(pyb.Pin.cpu.B6)
pin_B7 = pyb.Pin(pyb.Pin.cpu.B7)
setperiod = 65535
timer4 = pyb.Timer(4, prescaler=0, period=setperiod)

## Encoder driver object:
enc1 = Encoder(pin_B6, pin_B7, timer4, setperiod)

position_list = []
setpoint = 10000
kp = 0.8

## A KPControl object
actuation = KPControl(setpoint, kp)
motor1.enable()
motor1.set_duty(actuation.update(enc1.update()))

# trippoint prevents infinite loop in case setpoint isn't achieved
# might change while loop b/c this was mostly for testing
trippoint = 0
actuation_list = []
time_list = []

while (enc1.get_position() != setpoint) and (trippoint < 1000):
    motor1.set_duty(actuation.update(enc1.update()))
    position_list += [enc1.get_position()]
    time_list += [utime.ticks_ms()]
    # actuation_list.append(actuation.update(enc1.update()))
    utime.sleep_ms(10)
    trippoint += 1  # stop infinite loop
    # print(trippoint)

motor1.disable()
print(position_list)
print(actuation_list)

# utime.ticks ms()
# utime.ticks_diff(ticks1, ticks2)

# # infinite loop
# while True:
#   # question about positive feed?
#   user_in = input("Enter a Kp gain value pos or neg??????")






