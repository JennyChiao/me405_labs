## @file encoder.py
#  Brief doc for encoder.py
#
#  Detailed doc for encoder.py 
#
#  @author Jenny Chiao
#
#  @copyright License Info
#
#  @date May 5, 2020
#
#  @package encoder
#  Brief doc for the encoder module
#
#  Detailed doc for the encoder module
#
#  @author Jenny Chiao
#
#  @copyright License Info
#
#  @date May 5, 2020

import pyb

## An encoder driver object with functions:
# update, get_position, set_position, get_delta, and zero
#
#  @author Jenny Chiao
#  @copyright License Info
#  @date May 5, 2020
class Encoder:
    ''' This class implements methods for an encoder for the ME405 board.
        The parameters for an object of this are both encoder channel pin objects,
        the timer object, and the timer's set period. Methods available are
        update, get_position, set_posiion, get_delta, and zero'''

    ## Constructor for encoder driver
    def __init__(self, ENCA_pin, ENCB_pin, timer, period):
        ''' Sets up the Encoder class by initializing GPIOs and
        setting up the timer to encoder modes

        @param ENCA_pin   A pyb.Pin object to use as encoder A pin.
        @param ENCB_pin   A pyb.Pin object to use as encoder B pin.
        @param timer      A pyb.Timer object to use as an encoder counter
                          taking in signals from ENCA_pin and ENCB_pin
        @param period     The int value used in the pyb.Timer object's parameter'''
        print("Setting up encoder")

        self.ENCA_pin = ENCA_pin
        self.ENCB_pin = ENCB_pin
        self.timer = timer
        self.period = period

        self.timer.channel(1, pyb.Timer.ENC_A, pin=self.ENCA_pin)
        self.timer.channel(2, pyb.Timer.ENC_B, pin=self.ENCB_pin)

        # flag for update method
        self.flag = 0

        # variables to hold positions
        self.pos1 = self.timer.counter()
        self.pos2 = self.timer.counter()


    ## Updates recorded encoder position
    def update(self):
        ''' This method returns the updated/recorded encoder position as an int
        in the range from 0 to the number set by the timer's period'''
        if self.flag == 0:
            self.pos1 = self.timer.counter()
            self.flag = 1
            return self.pos1
        else:
            self.pos2 = self.timer.counter()
            self.flag = 0
            return self.pos2

    ## Gets the encoder's position
    def get_position(self):
        ''' This method returns the most recently updated encoder position'''
        if self.flag == 1:
            return self.pos1
        else:
            return self.pos2
    
    
    ## Sets the encoder's position
    def set_position(self, new_position):
        ''' This method resets the encoder's current position to a new_position value'''
        self.timer.counter(new_position)
        if self.flag == 0:
            self.pos1 = new_position
            self.flag = 1
        else:
            self.pos2 = new_position
            self.flag = 0
                


    ## Gets the change between the last two recorded positions from update()
    def get_delta(self):
        ''' This method returns the difference between the last positions recorded
            from update(). A flag is used as a toggle '''
        if self.flag == 1:
            delta = self.pos1 - self.pos2
            print("x1 - x2")
        else:
            delta = self.pos2 - self.pos1
            print("x2 - x1")

        # underflow detected
        if delta > self.period/2:
            delta += -self.period
            print("underflow delta > period/2")
        # overflow detected
        elif delta < -self.period/2:
            delta += self.period
            print("overflow delta < -period/2")
        
        return delta


    ## Zeros out the encoder
    def zero(self):
        '''Resets all position variables to 0'''
        self.pos1 = 0
        self.pos2 = 0
        self.timer.counter(0)


if __name__ == '__main__':

    # Setup for Motor 1 Encoder
    pin_B6 = pyb.Pin(pyb.Pin.cpu.B6)
    pin_B7 = pyb.Pin(pyb.Pin.cpu.B7)
    setperiod = 20
    timer4 = pyb.Timer(4, prescaler=0, period=setperiod)

    enc1 = Encoder(pin_B6, pin_B7, timer4, setperiod)

    # Setup for Motor 2 Encoder
    pin_C6 = pyb.Pin(pyb.Pin.cpu.C6)
    pin_C7 = pyb.Pin(pyb.Pin.cpu.C7)
    period = 65535
    timer8 = pyb.Timer(8, prescaler=0, period=setperiod)

    enc2 = Encoder(pin_C6, pin_C7, timer8, setperiod)