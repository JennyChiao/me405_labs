var main_8py =
[
    [ "actuation", "main_8py.html#ab1652bf26ef4638bd06bb3e24027e23a", null ],
    [ "enc1", "main_8py.html#a120ad412b4254af981df604b899a6ebf", null ],
    [ "file_count", "main_8py.html#a33aa9c727914cad1d125bede99e398fb", null ],
    [ "first_val", "main_8py.html#ab6735f9bebafa017aa4cdff19747e1c3", null ],
    [ "kp", "main_8py.html#af902ad7505407a7439de68bf877cd303", null ],
    [ "loop_count", "main_8py.html#a76b5120e0fc6b3a6f6f15e216962ce38", null ],
    [ "motor1", "main_8py.html#a3a2f088acede9b226beb08b70c42f146", null ],
    [ "pin_B6", "main_8py.html#acf716d0450f8dd1ed88acf2279a86855", null ],
    [ "pin_B7", "main_8py.html#ab73866d04d5aa16f2bb8beefae535d90", null ],
    [ "pin_en", "main_8py.html#ade16ca99eb65817a6359c8ee3de4f54a", null ],
    [ "pin_IN1", "main_8py.html#a537d3d4da75a61c087ac80386962f99e", null ],
    [ "pin_IN2", "main_8py.html#afb0a14e68f5dcbaa02d2d01bcd5bc447", null ],
    [ "position_list", "main_8py.html#aeef4581c64b729fb958a000aa80d13da", null ],
    [ "setperiod", "main_8py.html#aff4d2fef84cab5e8cba7516339c86f7d", null ],
    [ "setpoint", "main_8py.html#a424f2bc8c95a3a4452e0468c1f252aed", null ],
    [ "tim", "main_8py.html#a232f417b17e7cf7003f080763c8f511f", null ],
    [ "time_list", "main_8py.html#a4f2a5462c25b7192ef3ab6e075eb1026", null ],
    [ "timer4", "main_8py.html#a297136b16387c9a7d259e6697061af20", null ],
    [ "trippoint", "main_8py.html#ab5af46ebc9e79f514c25f1a95feee443", null ],
    [ "user_in", "main_8py.html#a514d6faf9b677b1a84e166a2b546f857", null ],
    [ "wait", "main_8py.html#a077c315f34dddfb415a9c766eb4e92b2", null ]
];