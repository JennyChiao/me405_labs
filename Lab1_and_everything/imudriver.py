## @file imudriver.py
#  Python source file to use the STM32 Nucleo-64 for STM32L476 board
#  for its timer ENC Mode to count encoder signals.
#
#  @package imudriver
#  Contains methods to enable/disable obtaining values from IMU, getting orientation values, 
#  and getting angular velocity values. Main contains a program to obtain and print
#  orientation values every second. 


import pyb
from pyb import I2C
import ustruct
import utime




## An IMU driver object with functions:
# enable, disable, get_orientation, and get_angvelocity

class IMU:
    ''' This class implements methods for an encoder for the ME405 board.
        The parameters for an object of this are both encoder channel pin objects,
        the timer object, and the timer's set period. Methods available are
        update(), get_position(), set_position(), get_delta(), and zero()'''

    ## Constructor for encoder driver
    def __init__(self):
        ''' Sets up the Encoder class by initializing GPIOs and
        setting up the timer to encoder modes'''
        print("Initializing I2C with IMU...")
        self.i2c = I2C(1, I2C.MASTER)  # create and init as a master
        # imu_addr = i2c.scan()
        # imu_addr = imu_addr[0]        # save slave addr
        self.imu_addr = 40
        self.i2c.init(I2C.SLAVE, addr=self.imu_addr)
        self.i2c.init(I2C.MASTER)
        ready = self.i2c.is_ready(self.imu_addr)

        if ready:
            print("Ready")
        else:
            print("Error, peripheral addr not setup correctly")

    def enable(self):
        '''BNO055 by default starts in Config Mode after power-on or RESET.
        This method sets the NDOF Fusion mode. '''
        self.i2c.init()
        print('IMU enabled')
        opr_mode = self.i2c.mem_read(1, self.imu_addr, 0x3D)  # reading from opr_mode
        
        if opr_mode != b'\x00':  # check that its in config mode
            self.i2c.mem_write(0x00, self.imu_addr, 0x3D)
        
        # set to NDOF Fushion mode
        opr_mode = self.i2c.mem_write(0x0c, self.imu_addr, 0x3D)
        
        
    def disable(self):
        ''' Disables getting readings from IMU by disabling I2C communication'''
        self.i2c.deinit()
        print('IMU disabled')

    def get_orientation(self):
        ''' This method gets the orientation as three Euler angles in degrees and returns them as a tuple.
        If the IMU is disabled it will not output values.'''
        try:
            hex_data = self.i2c.mem_read(6, self.imu_addr, 0x1A)
            vals = ustruct.unpack('<hhh', hex_data)
            vals = list(vals)
            for i in range(len(vals)):
                vals[i] = vals[i]/16
            return tuple(vals)
        except OSError:
            print("IMU disabled. Enable again to get orientation")

    def get_angvelocity(self):
        '''This method gets the three angular velocities (in degrees/sec) and returns
        them as a tuple. If IMU is disabled this method will not output values.'''
        try:
            hex_data = self.i2c.mem_read(6, self.imu_addr, 0x14)
            vals = ustruct.unpack('<hhh', hex_data)
            vals = list(vals)
            for i in range(len(vals)):
                vals[i] = vals[i]/16
            return tuple(vals)
        except:
            print("IMU disabled. Enable again to get angular velocity")


if __name__ == '__main__':
    '''Continuously queries the sensor and prints the Euler angles every second. '''
    imu = IMU()
    imu.enable()
    
    while True:              
        print("Euler Angles (degrees): ", imu.get_orientation())
        utime.sleep(1)
        
                

