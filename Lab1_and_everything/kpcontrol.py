## @file kpcontrol.py
#  Python module with input parameters for desired motor setpoint position and kp gain.
#  The
#
#  @package kpcontrol
#  Calculates error and actuation value for motor pwm


class KPControl:
	## Constructor for Kp Control
	def __init__(self, setpoint, kpgain):
		'''
		@param setpoint   Desired motor positon
		@param kpgain     Desired Kp gain value for proportional control
		'''
		self.setpoint = setpoint
		self.kpgain = kpgain

	## Updates the feedback system by calculating error and actuation
	def update(self, measured_loc):
		'''
		This method updates the feedback system by calculating error and actuation.
		The error is the difference between the desired setpoint and actual measured position.
		The actuation is calculated by multiplying the error by the Kp gain value.
		The output of this method is the actuation value.

		@param measured_loc   Measured position read from encoder.
		'''
		error = self.setpoint - measured_loc
		actuation = error * self.kpgain
		return actuation
