## @file motordriver.py
#  Python source file to use the Nucleo IHM04A1 motor driver
#
#  @package motor
#  Contains methods to drive the motor.
#
import pyb


class MotorDriver:
    ''' This class implements a motor driver for the ME405 board. '''

    def __init__(self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        
        @param EN_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on IN1_pin
                       and IN2_pin.  '''
        print("Creating a motor driver")
 
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        
        self.timch1 = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.timch2 = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)

        # Keep motor off initially
        self.EN_pin.low()
 
    def enable(self):
        ''' This method enables the motor driver to be used.
        The motor is initially set with 0% duty cycle.  '''
        print("Enabling Motor")
        
        self.timch1.pulse_width_percent(0)
        self.timch2.pulse_width_percent(0)
        self.EN_pin.high()

    def disable(self):
        ''' This method disables the motor driver.
        Setting the duty cycle will not move the motor
        while disabled.
        '''
        print("Disabling Motor")
        self.EN_pin.low()        

    def set_duty(self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in CCW direction, negative values
        in the opposite CW direction.
        @param duty A signed integer in the range -100 to + 100
        holding the duty cycle of the PWM signal sent to the motor '''
        
        # For 0% duty cycle turn both channels off to stop motor
        if duty == 0:           
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(0)

        # Timer Channel 1 positive dir +(1-100)# duty cycle for CW rotation
        elif duty > 0:            
            self.timch2.pulse_width_percent(0)
            self.timch1.pulse_width_percent(duty)
            
        # Timer Channel 2 negative dir -(1-100)% duty cycle for CCW rotation                
        else:
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(-duty)


if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    # Create the pin objects used for interfacing with the motor driver
    
    # Motor Driver CPU pins available:
    # EN/OCD = PA10
    # IN1 = PB4    TIM3 CH1
    # IN2 = PB5    TIM3 CH2
    #
    # or
    #
    # EN/OCD = PC1
    # IN1 = PA0    TIM5 CH1
    # IN2 = PA1    TIM5 CH2
    
    pin_en = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000)

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_en, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    moe.set_duty(10)
