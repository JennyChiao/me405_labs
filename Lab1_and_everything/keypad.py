## @file keypad.py
#  Python source file to use the STM32 Nucleo-64 for STM32L476 board
#  with a 4x4 16 key membrane keypad.
#
#  @package keypad
#  Contains methods to set and obtain position values a membrane keypad.
"""

Keypad module/class for MicroPython, using a timer callback (interrupt)
=======================================================================
Adapted from Brendan Simon's Code
https://github.com/BrendanSimon/micropython_experiments/blob/master/keypad/keypad_timer.py
Notes

"""

# !============================================================================

from pyb import Pin
from pyb import Timer
from pyb import delay


class Keypad():
    '''
    Keypad class used to scan a 4x4 16 key membrane keypad matrix. Utilizes callbacks.
    Contains the methods: get_key(), key_process(), scan_row_update(), start(), stop().
    The timer_callback() method contains the callback function when the timer interrupts.
    '''

    #Key states
    KEY_UP = const(0)
    KEY_DOWN = const(1)

    def __init__(self):
        self.init()

    def init(self):
        '''Constructor method initialises keypress char values, GPIO pins, key states, and timer 6.'''

        keys = [
            '1', '2', '3', 'A',
            '4', '5', '6', 'B',
            '7', '8', '9', 'C',
            '*', '0', '#', 'D',
        ]

        # Initialise keys to the UP state.
        self.keys = [{'char': key, 'state': self.KEY_UP} for key in keys]

        # Pin names for rows and columns.
        self.rows = ['PB3', 'PA10', 'PB5', 'PB4'] 
        self.cols = ['PB10', 'A8', 'PA9', 'PA7']

        # Initialise row pins as outputs.
        self.row_pins = [Pin(pin_name, Pin.OUT_PP) for pin_name in self.rows]

        # Initialise column pins as inputs.
        self.col_pins = [Pin(pin_name, mode=Pin.IN, pull=Pin.PULL_DOWN) for pin_name in self.cols]

        self.timer = Timer(6, freq=100)
        self.timer.deinit()
#         self.timer.callback(None)

        self.scan_row = 0
        self.key_code = None
        self.key_char = None

    # -------------------------------------------------------------------------

    def get_key(self):
        '''This method obtains and returns the most recent key pressed.'''

        key_char = self.key_char

        self.key_code = None  # consume last key pressed
        self.key_char = None  # consume last key pressed

        self.start()
        return key_char

    # -------------------------------------------------------------------------

    def key_process(self, key_code, col_pin):
        '''This method process a key press or release and returns the Up or Down state value. '''

        key_event = None

        if col_pin.value():
            if self.keys[key_code]['state'] == self.KEY_UP:
                key_event = self.KEY_DOWN
                self.keys[key_code]['state'] = key_event
        else:
            if self.keys[key_code]['state'] == self.KEY_DOWN:
                key_event = self.KEY_UP
                self.keys[key_code]['state'] = key_event

        return key_event

    # -------------------------------------------------------------------------

    def scan_row_update(self):
        '''
        Timer interrupt callback to scan next keypad row/column.
        NOTE: This is a true interrupt and no memory can be allocated !!
        '''

        # Deassert row.
        self.row_pins[self.scan_row].value(0)

        # Next scan row.
        self.scan_row = (self.scan_row + 1) % len(self.row_pins)

        # Assert next row.
        self.row_pins[self.scan_row].value(1)

    # -------------------------------------------------------------------------

    def timer_callback(self, timer):
        '''
        Timer interrupt callback to scan next keypad row/column.
        NOTE: This is a true interrupt and no memory can be allocated !!
        '''

        # key code/index for first column of current row
        key_code = self.scan_row * len(self.cols)

        for col in range(len(self.cols)):
            # Process pin state.
            key_event = self.key_process(key_code, self.col_pins[col])

            # Process key event.
            if key_event == self.KEY_DOWN:
                self.key_code = key_code
                self.key_char = self.keys[key_code]['char']

            # Next key code (i.e. for next column)
            key_code += 1

        self.scan_row_update()
#         print("done int")
        self.timer.deinit()

    # -------------------------------------------------------------------------

    def start(self):
        '''Starts the callback timer.'''
        self.timer.init(freq=90)
        self.timer.callback(self.timer_callback)

    # -------------------------------------------------------------------------

    def stop(self):
        '''Stop the callback timer.'''
        self.timer.deinit()
        self.timer.callback(None)


if __name__ == '__main__':
    # main used for testing

    keypad = Keypad()
    keypad.start()

    while True:
        key = keypad.get_key()
        if key:
            print("keypad: ", key)
        if key == "D":
            break
    keypad.stop()

