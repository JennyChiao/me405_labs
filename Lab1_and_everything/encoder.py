## @file encoder.py
#  Python source file to use the STM32 Nucleo-64 for STM32L476 board
#  for its timer ENC Mode to count encoder signals. 
#
#  @package encoder
#  Contains methods to set and obtain position values from a 2 channel encoder.


import pyb

## An encoder driver object with functions:
# update, get_position, set_position, get_delta, and zero
#
#  @author Jenny Chiao
#  @copyright License Info
#  @date May 5, 2020
class Encoder:
    ''' This class implements methods for an encoder for the ME405 board.
        The parameters for an object of this are both encoder channel pin objects,
        the timer object, and the timer's set period. Methods available are
        update(), get_position(), set_position(), get_delta(), and zero()'''

    ## Constructor for encoder driver
    def __init__(self, ENCA_pin, ENCB_pin, timer, period):
        ''' Sets up the Encoder class by initializing GPIOs and
        setting up the timer to encoder modes

        @param ENCA_pin   A pyb.Pin object to use as encoder A pin.
        @param ENCB_pin   A pyb.Pin object to use as encoder B pin.
        @param timer      A pyb.Timer object to use as an encoder counter
                          taking in signals from ENCA_pin and ENCB_pin
        @param period     The int value used in the pyb.Timer object's parameter'''
        print("Setting up encoder")

        self.ENCA_pin = ENCA_pin
        self.ENCB_pin = ENCB_pin
        self.timer = timer
        self.period = period

        # set encoder timer to use counter from encoder channels
        self.timer.channel(1, pyb.Timer.ENC_A, pin=self.ENCA_pin)
        self.timer.channel(2, pyb.Timer.ENC_B, pin=self.ENCB_pin)

        # initialize position variables
        # self.position contains the total position travelled and is positive for one direction and negative for the opposite direction. 
        self.position = self.timer.counter()
        
        # self.pos1 and self.pos2 respectively hold the current and previous timer counter values. 
        self.pos1 = self.timer.counter()
        self.pos2 = self.timer.counter()

    ## Updates recorded encoder position
    def update(self):
        ''' This method returns the updated/recorded encoder position as an int
        in the range from 0 to the number set by the timer's period'''
        self.pos2 = self.pos1
        self.pos1 = self.timer.counter()
        self.position += self.get_delta()
        return self.position

    ## Gets the encoder's position
    def get_position(self):
        ''' This method returns the most recently updated encoder position'''
        return self.position

    ## Sets the encoder's position
    def set_position(self, new_position):
        ''' This method resets the encoder's position variables to start at a new_position value'''
        self.position = new_position

    ## Gets the change between the last two recorded positions from update()
    def get_delta(self):
        ''' This method returns the integer difference between the last positions recorded
            from update(). A positive value indicates CW rotation. A negative value indicates
            CCW roation. Underflow is detected by checking if the delta difference is
            greater than half the timer period value. Overflow is detected by checking if the delta
            difference is less than half the negative timer period value. Underflow results in
            adding a negative timer period to delta and overflow results in adding a positive
            timer period to delta. '''
        delta = self.pos1 - self.pos2

        # underflow detected
        if delta > self.period/2:
            delta += -self.period
            print("underflow delta > period/2")
        # overflow detected
        elif delta < -self.period/2:
            delta += self.period
            print("overflow delta < -period/2")
        
        return delta

    ## Zeros out the encoder
    def zero(self):
        '''Resets all position variables and timer counter to 0'''
        self.position = 0
        self.pos1 = 0
        self.pos2 = 0
        self.timer.counter(0)


if __name__ == '__main__':
    # Main used for testing

    # Setup for Motor 1 Encoder
    pin_B6 = pyb.Pin(pyb.Pin.cpu.B6)
    pin_B7 = pyb.Pin(pyb.Pin.cpu.B7)
    setperiod = 200
    timer4 = pyb.Timer(4, prescaler=0, period=setperiod)
    
    # 1st encoder driver object:
    enc1 = Encoder(pin_B6, pin_B7, timer4, setperiod)

    # Setup for Motor 2 Encoder
    pin_C6 = pyb.Pin(pyb.Pin.cpu.C6)
    pin_C7 = pyb.Pin(pyb.Pin.cpu.C7)
    period = 65535
    timer8 = pyb.Timer(8, prescaler=0, period=setperiod)
    
    # 2nd encoder driver object
    enc2 = Encoder(pin_C6, pin_C7, timer8, setperiod)
    
    