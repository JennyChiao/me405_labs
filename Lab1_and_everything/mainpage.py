## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Spring 2020 ME405 Mechatronics Lab Files. This documentation contains the modules for Lab1 with the class MotorDriver,
#   Lab2 with the class Encoder, Lab3 main.py used to tune the motor system, and Lab4 with the class IMU. My team final project is also listed.
#
#  @section sec_pages Contents:
#  (click to open)\n
#  @ref lab1
#  \n
#  @ref lab2
#  \n
#  @ref page3
#  \n
#  @ref lab4
#  \n \n
#  @ref page4
#  \n
#  @ref finalproject
#  <hr>
#
#  @author Jenny Chiao
#
#  @copyright License Info
#
#  @date Last Updated: June 3, 2020
#
#
#
#  @page lab1 Lab 1 Motor Driver
#  @subsubsection Bitbucket
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/motordriver.py
#  \n
#  <hr>
#
#  @subsubsection Purpose
#  motordriver.py drives the motor through Nucleo IHM04A1 expansion board for brush DC motor driver.
#  /n This driver initializes the motor and contains methods to enable/disable the motor and set duty cycle.
# 
#  @subsubsection Usage
#  Micropython pyb library
#
#  @subsubsection Testing
#  Tested using A+ and A- ports on the expansion board with Timer 3 ch1 and ch2. Timer frequency set at 20000.
#  The motor was able to spin in either CW or CCW direction using the set_duty() method to
#  control speed and direction. 
#  
#  @subsubsection subsubsec_bugs Bugs and Limitations
#  Only tested at 20000 frequency. 
# 
#  \n \n
#  <hr>
#
#  @page lab2 Lab 2 Encoder Driver
#  @subsubsection Bitbucket
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/encoder.py
#  \n
#  <hr>
#  @subsubsection Purpose
#  encoder.py Drives an encoder through STM32 Nucleo-64 for STM32L476 board.
#  This driver contains methods to record/return positions, set a specified position, return distance travelled,
#  and reset the timer counter. 
# 
#  @subsubsection Usage
#  Micropython pyb library
#
#  @subsubsection Testing
#  Tested using period = 200 and period = 65535 on Timer 4 ch1 and ch2. Prescaler set to 0 to count
#  every encoder tick. Also tested with a second encoder to independently control both encoders.  
#  
#  @subsubsection subsec_bugs Bugs and Limitations
#  Overflow and underflow detection doesn't work if the absolute value of delta is greater than half the timer period.
#  \n \n
#  <hr>
#  
#  @page page3 Lab 3 Tuning Feedback Motor System
#  @subsubsection Bitbucket
#  main.py:
#  \n
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/main.py
#  \n \n
#  kpcontrol.py:
#  \n
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/kpcontrol.py
#  <hr>
#  @subsubsection Purpose
#  This program is used to tune the system to desired step response around a given setpoint by continually testing
#  different kp values. The setpoint is not user defined and is set at 10000 within main.py.
#
#  @subsubsection Tuning
#  The following YouTube link shows a video demonstration of the motor spinning until its
#  programmed setpoint is reached: \n
#  https://youtu.be/kCsQilBHr8U
#  \n \n
#  The system's Kp gain value was tuned by continually using the program to see what the resulting step response
#  would be. The time values and position values were saved to a text file on the MCU. Its values were copied
#  and pasted into an excel sheet to produce step response plots.
#  \n \n
#  The goal for this tuning was to get minimal overshoot and for the response to settle as close as possible to
#  the setpoint position of 10000 as quickly as possible.
#  \n \n
#  An initial value of Kp = 1 was tested:
#  @image html kpovershoot.JPG "Figure 1: Step response with overshoot"
#  Figure 1 shows that there is a little bit of overshoot and ripple around 1500 ms.
#  The red point is where the system settles at 9997, very close to the setpoint.
#
#  @image html kpundershoot.JPG "Figure 2: Step response with undershoot"
#  Figure 2 shows that this kp value is too low. The red dot shows the system falls short of 10000, setting at
#  9452 even with more data points than the previous test.
#
#  @image html kpbest.JPG "Figure 3: Step response close to ideal"
#  Figure 3 shows the final chosen kp value. There is minimal overshoot and the red dot shows the system is just
#  two ticks under the desired setpoint. For even better results, the slope and time to settle can be adjusted
#  with additional integration and derivative control added to the system.
#  \n \n
#  <hr>
#  @subsubsection Usage
#  Micropython pyb library\n
#  Micropython utime library
#
#  @subsubsection Testing
#  The program was tested extensively for functionality. The setpoint was tested at values as low as 1000
#  and as high as 50000. Kp value was tested at a maximum of 2 and a minimum of 0.0001. Negative values were
#  tested as well to see that the system would respond as expected.
#
#  @subsubsection subsec_bugs Bugs and Limitations
#  User is asked to input positive kp values but the system does except negative values. The negative values
#  result in a positive feedback response where the motor pwm increases to the motor's maximum speed. Its suggested
#  not to use any negative values because the motor will never reach the desired setpoint value.
#  \n \n
#  If the program is run from the mcu, the text files will save to its flash memory. Everytime the program is restarted
#  or closed and rerun, a new session starts and the old text files will be overwritten.
#
# 
#  \n \n
#  <hr>
#
#  @page lab4 Lab 4 IMU Driver
#  @subsubsection Bitbucket
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/imudriver.py
#  \n
#  <hr>
#  @subsubsection Video
#  https://youtu.be/RgvYDG4DYCY
#  \n
#  <hr>
#  @subsubsection Purpose
#  imudriver.py drives an I2C connected BNO055 IMU through STM32 Nucleo-64 for STM32L476 board.
#  This driver contains methods to enable and disable I2C communication to the IMU, get orientation Euler angle values in degrees, and
#  get angular velocity values in degrees/sec.
# 
#  @subsubsection Usage
#  Micropython pyb library
#
#  @subsubsection Testing
#  Printed out a paper protractor and placed the IMU in the center. The 0 degree angle print out was ligned up with 0 degrees on the
#  protractor. And as I moved a few degrees, made sure the printed euler angle also adjusted accordingly. This was done horizontally on the
#  table for x direction and also turned on the wall to test the y and z directions.
#  \n \n
#  The angular velocity was a little more difficult to test. I don't have an exact way of measuring how fast its going in degres per second.
#  The IMU was moved in each direction to see if the proper change was reflected in the corresponding tuple value.  
#  
#  @subsubsection subsec_bugs Bugs and Limitations
#  Simple methods implemented. No known bugs. Designed to only obtain the two types of values and only in NDOF mode.
#  \n \n
#  <hr>
#
#  @page page4 Project Proposal: Candle Extinguisher
#  @authors Jenny Chiao (Electrical Engineer)
#  \n Raul Gonzalez (Mechanical Engineer)
#  @date May 25, 2020
#  @subsection subsec_problem Problem Statement
#  Scented candles are a nice way to to set a relaxing environment for any room with its 
#  warm light and pleasant aromas. But they can be dangerous if a person falls asleep 
#  without putting the flame out. According to the National Fire Protection Association 
#  (NFPA),a 2013-2017 study showed about 1/3 of home candle fires start in the home 
#  bedroom. Falling asleep accounted for 11% of those incidents.
#  \n \n
#  Our project is a device that puts out the candle with just the push of remote control 
#  button. Multiple candles can be placed on a platform. There will be two motors 
#  utilized. One servo motor will be used to hinge a 3D printed arm holding a metal lid. 
#  The lid is lowered over the candle and held there for a set period of time to guarantee
#  the candle flames are extinguished from lack of oxygen. A stepper motor will be used on
#  a guide rail and belt system to horizontally move the lid holding arm to the correct 
#  candle location. An infrared remote controller for the user allows for an easy way to 
#  safely extinguish the candles from across the room. If the user is relaxed and 
#  comfortable, its simple to just press a button without too much thought. 
#  \n
#  <hr>
#
#  @subsection subsec_project Project Requirements
#  The STM32 Nucleo board running MicroPython will be used to control the motors and IR
#  sensor.
#  \n \n
#  The stepper motor and servo motors will serve as our two actuators for horizontal and 
#  rotational movement in the system. The stepper motor needs to move the arm and lid to 
#  the correct candle location. We may use an encoder with this for feedback position 
#  control. We may also decide to utilize the brushless dc motors from our lab kits if 
#  they are viable solutions that work with our required load. The servo motor will need
#  to lower the arm and lid until its placed over the candle. For this project we are 
#  assuming all candles will be the same size and height. 
#  \n \n
#  We are also utilizing an infrared receiver sensor to be used with a remote controller. 
#  Only two buttons will be used. The 1 button is used to turn off candle in position 1 
#  and 2 for candle in position 2.
#  \n
#  <hr>
#  @subsection Materials
#  For this project we will be using the Nucleo-STM32L476 board, the motor driver
#  shield, and at least one of the bread boards provided to us. The rest of the electrical
#  components needed will be purchased through Amazon or Digikey. The building material
#  for this project will be 3D printed and off the shelf components including a
#  linear guide rail system. The full list of all the material we intend to use is shown
#  in Table 1 in the bill of material subsection.
#  \n
#  @subsection subsec_bom Bill of Materials
#  @image html ibom_me_422.JPG "Figure 1: Initial Bill of Materals"
#
#  \n
#  <hr>
#
#  @subsection subsec_assemblyplan Assembly Plan
#  @subsubsection subsubsec_assem1 Sub assembly 1 (base components)
#  - Linear guide rail system \n
#  - 3d printed base for arm \n
#  - Wood piece \n
#  - Stepper motor \n
#  - 3d printed tower for ir sensor \n 
#  - 4? Bolts \n
#  - Mat for candle placement \n
#  @image html sub_assembly_1.JPG "Figure 2: Manufacturing steps for subassembly 1" width=50%
#  \n
#  \n \n
#  @subsubsection subsubsec_assem2 Sub assembly 2 (arm mechanism)
#  - 3D printed arm \n
#  - Metal Lid \n
#  - Pin \n
#  - Servo motor \n 
#  @image html sub_assembly_2.JPG "Figure 3: Manufacturing steps for subassembly 2" width=50%
#  \n
#  \n \n
#  @subsubsection subsub_assem3 Sub assembly 3 (electrical housing)
#  - 3D printed housing  \n
#  - Nucleo board \n
#  - DC motor shield \n 
#  @image html sub_assembly_3.JPG "Figure 4: Manufacturing steps for subassembly 3" width=50%
#  \n
#  <hr>
#
#  @subsection subsec_safety Safety Assessment
#  Working with lit candles is a hazard. We will be strategic to use materials that don’t
#  easily deform or melt and place them so they are out of the candle flame’s way. Our 
#  system will also be tested in a cleared space with all flammable materials out of the 
#  way. We’ll have a fire extinguisher and bucket of water ready in case anything happens.   
#  \n\n
#  We have one rotating component with the lid and arm mechanism that may create a
#  potential pinch point. Since the parts are not very heavy this shouldn’t cause any 
#  serious damage. The rail system may also cause damage if a hand or finger is on the 
#  rail while the mechanism is trying to move across. Again, parts are not very heavy but 
#  we will make sure to clear the rail and space around the system before testing. 
#
#  \n
#  <hr>
#
#  @subsection subsec_timeline Project Timeline and Tasks
#  We have a little under 3 weeks before the deadline of June 12.
#  We have been working together through Zoom throughout the quarter for each lab.
#  We will continue to do the same with this project and continue virtual meeting regularly
#  3 times a week for 2-3 hours each. Raul will be doing a lot of the mechanical
#  assembly and CAD models. Jenny will be doing electrical assembly. We'll both be collaborating
#  on writing the micropython code virtually. We will safely meet together for final system assembly. 
#  \n \n
#  Here is our plan:\n
#  @subsubsection subsubsec_may24 Week of May 24:
#  - Research and order parts by Tuesday at the latest \n
#  - Sketch CAD model of system \n
#  - Begin drafts for 3D printed parts \n
#  - Determine electrical pinouts for Nucleo board and peripherals\n
#  - Begin programming code for motors based on datasheets
#  \n \n
#
#  @subsubsection subsubsec_may31 Week of May 31: 
#  - Start 3D printing parts\n
#  - Testing/debugging motor driver codes on Nucleo board\n  
#  - Test servo motor with arm + lid attached on unlit candle (sub assembly 2) \n
#  - Test sub assembly 2 with a live candle and ensure nothing is melting \n 
#  - Assemble rail and belt system and test with stepper motor (sub assembly 1)\n
#  - Write code for IR remote controller and sensor \n
#  - Start writing main file for whole system \n
#  \n\n
#  @subsubsection subsubsec_june8 Week of June 8: 
#  - Finish testing IR sensor code and add sensor to system \n
#  - 3D print electrical housing \n
#  - Complete writing main system file by Tuesday \n
#  - Test/debug operation of entire system together with a live candle 
#  \n \n
#  <hr>
#  \n \n
#  <hr>
#
#  @page finalproject Final Project: Candle Extinguisher
#  @section ME405 Mechatronics Automatic Candle Extinguisher
#  \n
#  @image html test1.JPG "Figure 1: Final Bill of Materials"width=70%
#  \n
#  @authors Jenny Chiao (Electrical Engineer)
#  \n Raul Gonzalez (Mechanical Engineer)
#  @date June 12, 2020
#  \n
#  \n
#  @subsection Video
#  https://drive.google.com/file/d/1ZtMB4pJxjRhEfuMwgJV1vGijI44FYdXc/view
#  \n \n
#  This is our final demonstration video. The purple octopus hands are not part of the
#  system. They’re just helping hands to hold up the Nucleo board and wires out of the
#  way. In the video it looks like when Raul presses button 1, it moves the platform. But
#  it was just a coincidence that the platform was rehoming itself at the same time.
#  \n \n
#  The timing was shorted to seconds for this video demonstration but can be increased to
#  more realistic amounts of time before the candle is extinguished.
#  \n
#  \n
#  @subsubsection subsub_bloop Testing Bloopers!:
#  https://drive.google.com/file/d/1ZqX0EZ0QX4DLq6-5NgXhNOCOFxOLl-Yu/view?usp=sharing
#  \n \n
#  https://drive.google.com/file/d/1ZssSUfVfcG6FfSCMXeMYnkCceAJT1CGo/view?usp=sharing
#  \n \n \n
#  <hr>
#  @subsection subsec_bitbuck Bitbucket Code
#  (name.py are clickable)\n \n
#  main_finalproject.py
#  \n
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/main_finalproject.py
#
#  encoder.py
#  \n
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/encoder.py
#
#  motordriver.py
#  \n
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/motordriver.py
#
#  kpcontrol.py
#  \n
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/kpcontrol.py
#
#  Hall.py \n
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/Hall.py
#
#  keypad.py \n
#  https://bitbucket.org/JennyChiao/me405_labs/src/master/Lab1_and_everything/keypad.py
#  \n \n \n
#  <hr>
#  @subsection subsec_problem Revised Problem Statement
#  Scented candles are a nice way to to set a relaxing environment for any room with its
#  warm light and pleasant aromas. But they can be dangerous if a person falls asleep
#  without putting the flame out. According to the National Fire Protection Association
#  (NFPA),a 2013-2017 study showed about 1/3 of home candle fires start in the home
#  bedroom. Falling asleep accounted for 11% of those incidents.
#  \n \n
#  Our project is a device that puts out the candle with just the push of remote control
#  button. Multiple candles can be placed on a platform. There will be two brushed DC
#  motors utilized. Each motor has an integrated, attached encoder used for closed
#  feedback control. One motor will be used to hinge a 3D printed arm holding a metal lid.
#  The lid is lowered over the candle and held there for a set period of time to guarantee
#  the candle flames are extinguished from lack of oxygen. The second motor will be used
#  on a guide rail and thread screw system to horizontally move a platform with the lid
#  holding arm to the correct candle location. A hall sensor switch is also utilized with
#  a magnet to keep the platform calibrated properly. This way the candle locations are
#  always accurate even after repeated use. A numeric keypad is available for the
#  user to set a timer for either candle positions. After counting down for the set time,
#  the device automatically moves to the correct position to safely extinguish the candle.
#  If the user is relaxed and comfortable, its simple to just set up the candle and device
#  without too much thought.
#  \n \n \n
#  <hr>
#
#  @subsection subsec_project Revised Project Requirements
#  The STM32 Nucleo board running MicroPython will be used to control the motors, hall
#  switch sensor, and membrane keypad.
#  \n \n
#  The stepper motors with their encoders will serve as our two actuators for horizontal
#  and rotational movement in the system. The stepper motor needs to move the arm and lid
#  to the correct candle location using feedback position control. A hall effect sensor
#  with a magnet attached to the moving platform is used to recalibrate the home
#  position. For this project we are
#  assuming all candles will be the same size and height.
#  \n \n
#  We are also utilizing a membrane keypad.
#  Only two buttons will be used. The 1 button is used set the timer for a predefined
#  amount of time and 2 for candle in position 2.
#  \n \n \n
#  <hr>
#
#  @subsection Materials
#  For this project we will be using the Nucleo-STM32L476 board, the motor driver
#  shield, and at one of the bread boards provided to us. The rest of the electrical
#  components needed will be purchased through Amazon or Digikey. The building material
#  for this project will be 3D printed and off the shelf components including a
#  lead screw and guide rail system. The full list of all the material we intend to use is
#  shown in Table 1 in the bill of material subsection. Other miscellaneous materials not
#  listed in BOM: 22 gauge solid core wire, solder, and shrink wrap tubing.
#  \n \n
#  @subsection subsec_bom Bill of Materials
#  @image html test1.JPG "Figure 1: Final Bill of Materials"width=70%
#
#  \n \n \n
#  <hr>
#
#  @subsection subsec_safety Safety Assessment
#  Working with lit candles is a hazard. We will be strategic to use materials that don’t
#  easily deform or melt and place them so they are out of the candle flame’s way. Our
#  system will also be tested in a cleared space with all flammable materials out of the
#  way. We’ll have a fire extinguisher and bucket of water ready in case anything happens.
#  \n\n
#  We have one rotating component with the lid and arm mechanism that may create a
#  potential pinch point. Since the parts are not very heavy this shouldn’t cause any
#  serious damage. The rail system may also cause damage if a hand or finger is on the
#  rail while the mechanism is trying to move across. Again, parts are not very heavy but
#  we will make sure to clear the rail and space around the system before testing.
#
#  \n \n \n
#  <hr>
#
#  @subsection subsec_hardware Hardware
#
#  Our system uses a leadscrew and rail kit as shown in Figure 3: \n
#  @image html me405_leadscrew.jpg "Figure 3: Amazon.com picture of leadscrew kit"width=25%
#  \n
#  The mounted platform base and hinging arm were 3D printed with custom created
#  Solidworks models shown in Figure 4 and Figure 5. Figure 6 shows the 3D model of the
#  complete assembly.  \n
#  @image html test1.jpg "Figure 4: 3D model of platform base"width=30%
#  \n
#  @image html me405_arm.jpg "Figure 5: 3D model of arm"width=30%
#  \n
#  @image html me405_complete_assembly.jpg "Figure 6: 3D model completed assembly"width=30%
#  \n \n \n
#  Figure 7 below shows the system pinouts used for wiring to peripherals and motors. \n
#  @image html me405_pinoutschematic.jpg "Figure 7: Electrical hardware wiring schematic"width=40%
#  \n \n \n
#  <hr>
#
#  @subsection subsec_software Software
#  @subsubsection subsubsec_main Main
#  The main file main_finalproject.py contains tasks defined by classes. We chose not
#  to put them in separate python modules so that we could use global flags between tasks.
#  \n \n
#  The primary function of main is to continuously run through the task loop as a
#  round-robin scheduler for multitasking implementation.
#  \n \n
#  The 8 tasks in the loop are described below from their state diagrams.
#
#  @subsubsection subsubsec_taskbtn Task: CheckBtn
#  Continuously checks and saves a keypress from the membrane keypad. \n
#  @image html me405_button.JPG "Figure 8: CheckBtn Task State"width=35%
#  \n
#  \n \n
#  @subsubsection subsubsec_tasktimers Tasks: Timer1, Timer2, and HoldLidTimer
#  When proper flags are set, timers count to predefined values before setting flags for
#  subsequent tasks to move through their states.\n
#  @image html me405_timers.JPG "Figure 9: Timer Task States"width=35%
#  \n
#  \n \n
#  @subsubsection subsubsec_taskmovetocandle Task: MoveToCandle
#  Flags from Timer1 and Timer 2 initialize the state to start for either candle in
#  position 1 or in position 2. If candle 1 and candle 2 timers finish before one of the
#  candles is extinguished, then candle 1 gets taken care of first. Candle 2 gets
#  extinguished afterwards. \n
#  @image html test1.JPG "Figure 10: MoveToCandle Task States"width=35%
#  \n
#  \n \n
#  @subsubsection subsub_taskmovelid Task: MoveLid
#  Flag from MoveToCandle task activates MoveLid states to close the candle lid. \n
#  @image html me405_movelid1.JPG "Figure 11: MoveLid Task States for closing lid"width=35%
#  \n
#  \n
#  Flag from HoldLidTimer activates the MoveLid states to open the candle lid. \n
#  @image html me405_movelid2.JPG "Figure 12: MoveLid Task State for opening lid"width=35%
#  \n \n
#  @subsubsection subsub_taskgohome Task: GoHome
#  On first device startup is initialized to check for home position if the platform is
#  not already there. This task is also activated after MoveLid opens the lid and sets the
#  flag to return to home position. \n
#  @image html me405_gohome.JPG "Figure 13: GoHome Task State"width=35%
#  \n
#  \n \n
#  <hr>
#
#  @subsection Results
#  We have a little under 3 weeks before the deadline of June 12.
#  We have been working together through Zoom throughout the quarter for each lab.
#  \n
#  @subsubsection subsub_limits Limitations and Debugging
#  The debugging process of this code involved testing the code for each individual class
#  on its own to ensure that the code would work. Next, came incorporating the
#  individual classes to the main code one at a time. The first code tested in
#  this manner was the timers.
#  \n \n
#  Once, the timers were set up the next thing that was
#  tested was the lead screw motor and the going home code. The lead screw was not
#  attached while testing to see if the motor would stop once the hall effect sensor was
#  triggered. This code worked after minor tweaks. The lead screw was left
#  disconnected from the motor to ensure the other sections of the code would not cause a
#  issue with the lead screw motor.
#  \n \n
#  The next code section that was tested was the code for the keypad. The keypad code on
#  its own worked perfectly. However, when incorporated with the going home code it
#  caused some problems with the lead screw motor. When the keypad code was looking for
#  keys being pressed the lead screw motor would to continue to run even after the hall
#  effect sensor was triggered. We were unable to fix this issue even when disabling the
#  keypad timer or changing the timer used for the keypad. We chose not run this code
#  moving forward, but still demonstrated how the keypad would have functioned. With the
#  rest of the code.
#  \n \n
#  The code for moving the platform to position 1 was tested next without connecting to
#  the lead screw yet. The lead screw motor was tested to see if it would stop at its
#  intended encoder position. We then connected the lead screw. The setpoint on the
#  lead screw motor had to be changed in order to get the motor spinning the right way.
#  Once that was set, the timers and the go home code was tested together. It worked as
#  intended . A similar process was taken when testing the second position.
#  \n \n
#  The final thing that was tested was the closing lid motor. Again the motor was tested
#  without its attachment. The setpoint for the motor had to be changed to a negative
#  value to ensure that it rotated in the right direction. We chose set point values that
#  looked about right in during the testing. This went well until the lid was attached.
#  One of the gears on the mini torque box was stripped because the set point value was
#  not set correctly and tried to stop the arm from falling but the force from the lid
#  falling was too great and stripped the gear. The motor was changed as well as the
#  setpoint value. It worked once that change was made.
#  \n \n
#  That was the final step before the candles were set up and lit. The first few times we
#  tired putting out the candle the lid did not flip into the correct position. Hot
#  glue was used to keep the lid in position and tried one more time. It worked as a
#  intended with this fix
#  \n \n
#  It should be noted that when testing with the motors connected to their respective
#  components  one of us kept a finger on the on/off switch to the power outlet strip in
#  case something went wrong. It should also be noted that code was always run to disable
#  the motors before running the full code.
#  \n \n
#
#  @subsubsection subsub_futureworks Future Works
#  If we had more time to work on this project we would have liked to incorporate an LED
#  screen to add custom times to turn the candles off. Quieter motors would be looked at
#  because the lead screw motor was very loud. A servo motor would be used for the arm to
#  better control the movement of the lid. Housing for all the components would be
#  designed and 3D printed.
#  \n
#  \n \n
#  <hr>
#
#  @subsection Acknowledgements
#  A shoutout to our friend Marco Salazar (Cal Poly Aerospace Engineering Student) for
#  letting us use his 3D printer and for consulting with us on the parameters we have to
#  work with. Your expertise added to our learning experience and inspired us to utilize
#  3D printing more in our future projects!
#  \n \n
#  Also a thank you to Rodrigo Gonzalez (Cal Poly Mechanical Engineering Student) for
#  assisting with smaller tasks such as soldering and brainstorming ideas with us. It was
#  rewarding helping you get a glimpse of mechatronics and senior year engineering.
#  Hope this project got you motivated to keep going!
#  \n \n
#  Cal Poly Mechanical Engineering Department thank you for supplying us with our ME405
#  kits!!! And thank you to our mechatronics professor Charlie Refvem for his patience
#  and helpfulness during a strange and chaotic time that was our virtual Spring 2020
#  quarter!
#  \n \n
#  Last but not least we want to give credit to Brendan Simon for sharing a public Github
#  code using the same membrane keypad we utilized. We adapted and learned from his code
#  found here: \n
#  https://github.com/BrendanSimon/micropython_experiments/blob/master/keypad/keypad_timer.py
#  \n \n \n