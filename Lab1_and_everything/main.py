## @file main.py
#  Python main program that implements a feedback control tuning system using modules motordriver.py, encoder.py,
#  and kpcontrol.py.
#  Running the program displays a predetermined encoder setpoint and asks the user for a positive Kp gain value.
#  The resulting time(ms) and position values are printed out in two columns and the user is given the option
#  to save the data to a text files on the mcu/computer directory. The user is then asked if they want to compute
#  another step response.


import utime
import pyb
from motordriver import *
from encoder import Encoder
from kpcontrol import KPControl

# Initializing motor pins
pin_en = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000)

## A motor driver object
motor1 = MotorDriver(pin_en, pin_IN1, pin_IN2, tim)

# Initializing encoder pins
pin_B6 = pyb.Pin(pyb.Pin.cpu.B6)
pin_B7 = pyb.Pin(pyb.Pin.cpu.B7)
setperiod = 65535
timer4 = pyb.Timer(4, prescaler=0, period=setperiod)

## Encoder driver object:
enc1 = Encoder(pin_B6, pin_B7, timer4, setperiod)

## Initialize variables
# trippoint represents approx milisecond value for how long data is recorded
trippoint = 500
loop_count = 0
# actuation_list = []   # only used for testing/debugging
time_list = []
position_list = []
kp = 0
file_count = 1
setpoint = 10000    # desired motor position

## Driver Code Loop.
'''The user is continually prompted for Kp value. The resulting step response values, 
Time(ms) and Position are printed out as a two column, comma separated list. 
The user is prompted to save the file or not. And the prompted again if another set of data 
is to be calculated. The program loops back around to asking for a Kp value. 
'''
while True:
    print("\nSetpoint = ", setpoint)
    kp = input("Enter a positive Kp gain value: ")
    kp = float(kp)

    actuation = KPControl(setpoint, kp)
    motor1.enable()
    motor1.set_duty(actuation.update(enc1.update()))

    while loop_count < trippoint:
    # while (enc1.get_position() != setpoint) and (trippoint < 700):
        # update lists
        position_list += [enc1.update()]
        time_list += [utime.ticks_ms()]

        # send actuation signal to motor pwm
        motor1.set_duty(actuation.update(enc1.get_position()))
        # wait 10ms
        utime.sleep_ms(10)
        loop_count += 1  # stop infinite loop

    # Turn motor off for safety
    motor1.disable()

    print("Saving time and position data...")
    # Change time_list to start at 0
    first_val = time_list[0]
    for i in range(len(time_list)):
        time_list[i] -= first_val

    # print out two columns for time and position
    print("\nTime(ms), Position")
    for i in range(len(time_list)):
        print(time_list[i], ',', position_list[i])

    user_in = input("\n\nWould you like to save this step response data? \nThis will "
                    "overwrite previous session's files. (y)es or (n)o: ")

    # write data to text file on mcu
    if user_in == 'y':
        print("Saving file...")
        with open("datalist" + str(file_count) + ".txt", 'w') as file:
            file.write("kp = " + str(kp) + '\n')
            file.write("setpoint = " + str(setpoint) + '\n')
            file.write("Time(ms)\tPosition\n")
            for i in range(len(time_list)):
                file.write(str(time_list[i]) + '\t' + str(position_list[i]) + '\n')
        print("\ndatalist.txt saved in directory")

        # incr file count so multiple files in a session can be saved without overwriting
        file_count += 1

    # do nothing otherwise
    elif user_in == 'n':
        pass
    else:
        print("Invalid response. File not saved.")

    # Ask if another step response is wanted
    wait = 1
    while wait:
        user_in = input("\n\nWould you like to compute another step response? (y)es or (n)o: ")
        if user_in == 'n':
            print("quitting...")
            break
        elif user_in == 'y':
            wait = 0
        else:
            print("invalid response")
    if wait == 1:
        break

    # RESET EVERYTHING BEFORE MOVING ON
    loop_count = 0   # reset loop count
    enc1.zero()
    time_list = []
    position_list = []
