''' @file main.py
There must be a docstring at the beginning of a Python source file
with an @file [filename] in it! '''

def fib (idx):
	''' This method calculates a Fibonacci number corresponding to
	a specified index.
	@param idx An integer specifying the index of the desired
	Fibonacci number. '''
	print ('Calculating Fibonacci number at '
		   'index n = {:}.'.format(idx))

	if __name__ == '__main__':
		# Replace the following statement with the user interface code
		# that will allow testing of your Fibonacci function. Any code
		within the if __name__ == '__main__' block will only run when
		# the script is executed as a standalone program. If the script
		# is imported as a module the code block will not run.
		fib(0)