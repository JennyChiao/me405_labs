## @file Lab0_fibcalc.py
# ME405 Lab 0 Assignment. Prompts user to input a valid index to print out the 
# corresponding Fibonacci number. 
# 
# Created on Wed Apr  8 15:41:43 2020
# @author: Jenny Chiao



def fib(idx):
    """ This method calculates a Fibonacci number corresponding to
    a specified index.
    @param idx An integer specifying the index of the desired
                Fibonacci number."""

    print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))

    '''Follows the formula fn = f(n-1) + f(n-2) for index greater than 1
       if larger 1'''
    n, ans = 0, 1

    if idx < 2:
        ans = idx

    else:
        for i in range(idx-1):
            n, ans = ans, n + ans
    return ans


## String contains user input to be parsed for deciding to calculate another
# Fibonacci number or not.
user_in = input("Would you like to calculate a Fibonnaci number? (y)es or (n)o: ")

if __name__ == '__main__':
    while True:

        if user_in == 'n':
            print("Ok. Goodbye!")
            break

        elif user_in == 'y':
            ## String contains user index number input for calculating fibonacci
            user_idx = input("Enter Fibonnaci integer index: ")

            # Check that user_idx is a postive integer digit
            if user_idx.isdigit():
                ## fib_ans converts string input to int type and uses method fib()
                fib_ans = fib(int(user_idx))
                print("\nFibonacci number at index n = {:} is {:}".format(user_idx, fib_ans))

            else:
                print("\nERROR! Invalid input. Please enter 0 or positive integer.")

        user_in = input("\nWould you like to calculate another Fibonnaci number? (y)es or (n)o: ")
