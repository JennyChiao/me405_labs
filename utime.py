def ticks_ms():
	"""
    Returns an increasing milisecond counter with arbitrary reference point that wraps
    around after some value.
    """
	pass


def sleep_ms(ms: object) -> object:
	"""
	Delay for given number of microseconds, should be positive or 0
	"""
	pass


def ticks_diff(ticks1, ticks2):
	"""
	Measure ticks difference between values returned from ticks_ms() function
	as a signed value which may wrap around.
	"""
	pass
